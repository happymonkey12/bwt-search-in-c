/*
 * COMP9319 ASSIGNMENT 3 - BWT SEARCH
 * BY JIMMY JUN QIAN (jqia001 / z3418175)
 */


#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include "dataStructs.h"
#include <stdlib.h>
/*
 * Constants
 */
#define CHARS_TO_READ 500000 //500kb
#define PATTERN_START 4 //First pattern is argv[4]

/*
 * Function prototypes
 */
void gen_index_file(char *);
int occ(int, char);
void gen_c_table(void);
void print_c_table(void);
first_last_t backwardSearch(char *);
record_t recordPosition(int);
char readIthChar(int);
void print_record(record_t);
void and_the_lists(SL, SL);
void printListSize(SL);
void exit_program(void);

/*
 * Global variables
 */

static FILE *bwtFile; //The BWT file to search
static FILE *indexFile; //The index file
static int bwtSize;  //Size of the BWT file
static int cTable[128]; // The C function array
static int PATTERN_END;
static int num_patterns;

int main(int argc, char **argv) {
  bwtFile = fopen(argv[1], "rb");
  if (bwtFile == NULL) {
    printf("BWT file failed to open\n");
    return 1;
  }
  
  fseek(bwtFile, 0, SEEK_END);
  bwtSize = ftell(bwtFile);
  fseek(bwtFile, 0, SEEK_SET);
  
  /*
   * Index file needed only if bwtFile size >= 1024
   */
  if (bwtSize >= 1024) {
    /*
     * If file exists, open it. If not, make it
     * name of indexFile is argv[2]
     */ 
    indexFile = fopen(argv[2], "rb");
    //make the index file if it doesnt exist
    if (indexFile == NULL) {
      gen_index_file(argv[2]); 
      indexFile = fopen(argv[2], "rb");
    } 
    
    assert(indexFile != NULL);
  } else { //If size of BWT is < 1024, we never use the index file
    indexFile = NULL;
  }

  /*
   * Index the patterns
   */
  PATTERN_END = argc - 1;
  num_patterns = PATTERN_END - PATTERN_START + 1;
  /*
   * Patterns stored in argv[PATTERN_START...PATTERN_END] inclusive
   * Generate C table
   */
  gen_c_table();
  

  /******************************************
   * Search Pattern 1 ---> primeList
   *********************************************/
  first_last_t r1 = backwardSearch(argv[PATTERN_START]);
  SL primeList = newList();
  
  /*
   * Store records from search1 in PRIMELIST
   * STore only legit records !! 
   */ 
  int i;
  record_t currRec;
  for (i = r1.first; i <= r1.last; i++) {
    currRec = recordPosition(i);
    if (currRec.pos != -1) { //Insert only if valid
      insert(primeList, currRec);
    } 
  }
  /* Print nothing if there are no matches since for a record to be 
   * printed it MUST exist in every list
   */
  if (isEmpty(primeList) == 1) {
    exit_program();
  }
  /*******************************************************************
   * For EACH additional pattern 
   *      - perform backwardSearch and store result in a first_last_t
   *      - Do loop similar to above to store result in a secondaryList
   *      - Perform andTheLists(prime, secondary) which updates prime
   *        to store only common records between prime and secondary
   *******************************************************************/
  
  // printf("S: %d E: %d\n", PATTERN_START, PATTERN_END);

  SL secondaryList = NULL;
  int j;
  for (i = PATTERN_START+1; i <= PATTERN_END; i++) {
    
    //    printf("DOING BACKWARDWARDS SEARCH FOR %d\n", i);
    
    r1 = backwardSearch(argv[i]);
    secondaryList = newList();
    assert(secondaryList != NULL);
    
    for (j = r1.first; j <= r1.last; j++) {
      currRec = recordPosition(j);
      if (currRec.pos != -1) {
	insert(secondaryList, currRec);
      }
    }
    //Can do nothing if theres no match
    if (isEmpty(secondaryList) == 1) {
      exit_program();
    }
    
    //Both gauranteed to be non empty
    and_the_lists(primeList, secondaryList);

    //Can do nothing if theres no match
    if (isEmpty(primeList)==1) {
      exit_program();
    }

    
    deleteList(secondaryList);
    secondaryList = NULL;
  }
  




  /*
   * Prime list now contains all common records (across all search queries)
   * Print each record in primeList
   */
  if (size(primeList) == 0) {
    exit_program();
  }
  
  /*
   * argv[3] is the MAX number of records to print
   */
  int printXrecords = size(primeList);
  if (atoi(argv[3]) < printXrecords) {
    printXrecords = atoi(argv[3]);
  }

    
  for (i = 0; i < printXrecords; i++) {
    print_record(get(primeList, i));
    printf("\n");
  }
  //printListSize(primeList);


  if (indexFile != NULL) {
    fclose(indexFile);
  }
  fclose(bwtFile);
  return 0;
}

/*
 * generate C[c] table...cTable[c]
 */
void gen_c_table(void) {
  int charOcc[128] = {0};
  char charStream[CHARS_TO_READ];
  int charsRead = 0;
  int lucy = 0;
  int i;
  fseek(bwtFile, 0, SEEK_SET);  
  /*
   * First get the occurence of each character in bwtFile & store in charOcc[]
   * Read 4,000,000 characters at once (4 megabytes)
   */
  while (charsRead != bwtSize) {
    lucy = fread(charStream, sizeof(char), CHARS_TO_READ, bwtFile);
    charsRead += lucy;
    for (i = 0; i < lucy; i++) {
      charOcc[(int)charStream[i]]++;
    }
  }

  /*
   * All characters in the BWT file have been read
   * Now fill in cTable[i] 
   */
  cTable[0] = 0;
  for (i = 1; i < 128; i++) {
    cTable[i] = cTable[i-1] + charOcc[i-1];
  }
  
}

void print_c_table(void) {
  int lucy;
  printf("Printing C[c]\n");
  for (lucy = 0; lucy < 128; lucy++) {
    printf("%c: %d\n", (char)lucy, cTable[lucy]);
  }
}

/*
 * Occurence function
 * if pos is LESS THAN 1024, fread pos characters from bwtFile
 * else fseek to (rd.pos/1024 - 1) * sizeInt * 128 + c * sizeInt
 * read that value, then read remaining X characters from bwtFile
 */
int occ(int pos, char c) {
  //the ranking of C at pos 0 is 0
  if (pos == 0) {
    return 0;
  }


  /*
   * pos is LESS THAN 1024
   * Read the first pos characters into bwtStream
   */
  if (pos < 1024) {
    fseek(bwtFile, 0, SEEK_SET);
    char bwtStream[1024];
    assert((fread(bwtStream, 1, pos, bwtFile)) == pos);
    
    /*
     * bwtStream[0..pos-1] now contains first pos characters of bwtFile
     * Count all the occurences of c from 0...pos-1
     */
    int occurence = 0;
    int i;
    for (i = 0; i < pos; i++) {
      if (bwtStream[i] == c) {
	occurence++;
      }
    }
    return occurence;
  }
  
  /*
   * pos >= 1024
   */
  
  int row = ((int)floor(pos/1024.0)) - 1;
  int posInIndex = row*sizeof(int)*128 + ((int)c)*sizeof(int);
  fseek(indexFile, posInIndex, SEEK_SET);
  int smallOcc[1];
  assert(fread(smallOcc, sizeof(int), 1, indexFile) == 1);
  int occurence = smallOcc[0];
  int nthCharInBwt = ((int)floor(pos/1024.0))*1024;
  /*
   * If occ(pos) is stored in index file, return it
   */
  if (nthCharInBwt == pos) {
    return occurence;
  }
  int numCharsToRead = pos - nthCharInBwt;
  char bwtStream[1024];
  fseek(bwtFile, nthCharInBwt, SEEK_SET);
  assert(fread(bwtStream, 1, numCharsToRead, bwtFile) == numCharsToRead);
  int i;
  
  /*
   * Update occurence 
   */
  for (i = 0; i < numCharsToRead; i++) {
    if(bwtStream[i] == c) {
      occurence++;
    }
  }
  return occurence;
}

/*
 * File is >= 1024 bytes. Generate the index file.
 */
void gen_index_file(char *name) {
  FILE *fp = fopen(name, "wb");
  assert(fp != NULL);
  /*
   * Create temporary occ[128]
   * Read 1024 characters from BWT into charSet[1024]
   * for each c in charSet increase occ[c] by 1
   * write occ to indexFile
   * keep doing this until you read less than 1024 from BWT, in which case
   * we're done
   */
  int i;
  int tempOcc[128] = {0};
  char charSet[1024];
  fseek(bwtFile, 0, SEEK_SET);
  while ( fread(charSet, sizeof(char), 1024, bwtFile) == 1024 ) {
    for (i = 0; i < 1024; i++) {
      tempOcc[(int)charSet[i]]++;
    }
    fwrite(tempOcc, sizeof(int), 128, fp);
  }
  
  fclose(fp);

}
/*
 * Returns the positions of p in BWT 
 * Pointers to one character preceding p 
 */
first_last_t backwardSearch(char *p) {
  
  /*
   * Gotta handle a search for '['
   * Return a negative since there arn't any [
   */
  if (p[0] == '[') {
    first_last_t neg;
    neg.first = 21;
    neg.last = 12;
    return neg;
  }
  
  int i = strlen(p);
  char c = p[i-1];
  int first = cTable[(int)c] + 1;
  int last = cTable[(int)c + 1];

  while ((first <= last) && i >=2) {
    c = p[i-2];
    first = cTable[(int)c] + occ(first-1, c) + 1;
    last = cTable[(int)c] + occ(last, c);
    i--;
  }
  
  /*
   * If last < first, then there is no match, but we return it anyway
   * as it will be interpreted in function callee
   */
  first_last_t res;
  res.first = first;
  res.last = last;
  
  return res;
  
}

/*
 * Given a character position in BWT, keep backward searching until 
 * a '[' is found. -----> Return the position of this '[' <---------
 *
 * Since numbers are a valid searh pattern, we must ensure that
 * the search for '[' first encounters a ']' in case the 3 is part of [3]
 * If that's the case, return -1 for handling in caller function
 *
 * This function also has to return the record number since records have to be
 * output in order. Hence we return a struct
 */


record_t recordPosition(int p) {
  /*
   * The next fgetc call will return the p'th character in BWT
   * This is the character PRECEDING the search term.
   */
  fseek(bwtFile, p-1, SEEK_SET);
  char c = fgetc(bwtFile);
  int lucy = 0;
  int record_num = 0;
  int multiplier = 1;
  
  while (c != '[') {
    /* Lucy being one means that the current character is part of 
     * the record number, so we must store it
     */
    if (lucy == 1) {
      assert( (c >= '0') && (c <= '9') ); //make sure we are inside the record
      record_num += multiplier * (c - '0');
      multiplier = multiplier * 10;
    }

    /*
     * Confirming that the record is legitimate
     */
    if (c == ']') {
      lucy = 1;
    }
    
    p = cTable[(int)c] + occ(p, c);
    fseek(bwtFile, p-1, SEEK_SET);
    c = fgetc(bwtFile);
    
    assert(c != EOF); //If this happens we have problem
  }

  /*
   * Check legitimacy of record
   * -1 -1 for not legit
   */
  record_t rec;
  
  if (lucy != 1) {
    rec.pos = -1;
    rec.number = -1;
  } else {
    rec.pos = p;
    rec.number = record_num;
  }
  
  return rec; 
}

/*
 * Given a record, print the entire thing using BWT forward search
 */
void print_record(record_t r) {
  int stop = 0;
  int pos = r.pos;
  char c = readIthChar(pos);
  int ch;
  int rank = 0;
  int currPos, start, end, currRank;

  /*
   * While the new '[' hasn't been read, keep forward searching & printing
   */
  while (stop == 0) {
    putchar(c);

    /*
     * Search for character corresponding to pos in F
     */
    ch = 0;
    while (cTable[ch] < pos ) {
      ch++;
    }
    rank = pos - cTable[ch-1]; // This is the rank of c in BWT
    c = (char)(ch-1);
    
    /*
     * Binary search
     */
    start = 1;
    end = bwtSize;
    while (start <= end) {
      currPos = (int)floor((start+end)/2);
      currRank = occ(currPos, c);
      if (currRank == rank) {
	pos = currPos;
	end = currPos-1;
      } else if (currRank < rank) {
	start = currPos + 1;
      } else {
	end = currPos - 1;
      }
    }

    /*
     * c is the next character and pos is the BWT position of c 
     */
    if (c == '[') {
      stop = 1;
    }

  }
  
  return;
}

/*
 * i = 1 ...bwtSize
 */
char readIthChar(int i) {
  assert(i > 0);
  assert(i <= bwtSize);
  
  fseek(bwtFile, i-1, SEEK_SET);
  return fgetc(bwtFile);
}


/*
 * Stores all common elements in prime
 * Both lists are not empty
 */
void and_the_lists(SL prime, SL sec) {

  assert((prime!=NULL) && (sec!=NULL));
  int primeSize = size(prime);
  int secSize = size(sec);
  int i = 0;
  int j = 0;
  int numDeleted = 0;
  int primeValue = 0;
  int secValue = 0;
  int primeExhausted = 0;
  
  while (1) {
    if (i+numDeleted == primeSize) {
      primeExhausted = 1;
      break;
    }
    
    if (j == secSize) {
      break;
    }
    
    primeValue = get(prime, i).number;
    secValue = get(sec, j).number;
    if (primeValue == secValue) {
      i++;
      j++;
    } else if (primeValue < secValue) {
      delete(prime, i);
      numDeleted++;
    } else {
      j++;
    }
  }
  
  if (primeExhausted == 1) {
    return;
  }
  
  /*
   * Delete the rest of the elements in prime
  */
  int howMany = size(prime) - i;
  int lucy = 1;
  while (lucy <= howMany) {
    delete(prime, i);
    lucy++;
  }
  
  return;
}
	 
void printListSize (SL l) {
  printf("%d\n", size(l));
}

void exit_program(void) {
  if (indexFile != NULL) {
    fclose(indexFile);
  }
  fclose(bwtFile);
  exit(0);
}
