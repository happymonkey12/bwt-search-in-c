#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>

/*
 * Function prototypes
 */
void gen_index_file(char *);
int occ(int, char);

/*
 * Global variables
 */
static FILE *bwtFile;
static FILE *indexFile;
static int bwtSize;


int main(int argc, char **argv) {
  bwtFile = fopen(argv[1], "rb");
  if (bwtFile == NULL) {
    printf("BWT file failed to open\n");
    return 1;
  }
  
  fseek(bwtFile, 0, SEEK_END);
  bwtSize = ftell(bwtFile);
  fseek(bwtFile, 0, SEEK_SET);
  
  /*
   * Index file needed only of bwtFile size >= 1024
   */
  if (bwtSize >= 1024) {
    /*
     * If file exists, open it. If not, make it
     * name if indexfile is ifName
     */
    char ifName[100];
    char *ind = ".ind";
    strcpy(ifName, argv[1]);
    strcat(ifName, ind);
    
    indexFile = fopen(ifName, "rb");
    //make the index file if it doesnt exist
    if (indexFile == NULL) {
      printf("Generating idnex file\n");
      gen_index_file(argv[1]);      
      indexFile = fopen(ifName, "rb");
    } else {
      printf("Index file exists\n");
    }
    assert(indexFile != NULL);
    //So now we have index file: indexFile
  }
  
  /*
   * TESTS - for bwtFiles/yahoo.bwt
   */

  /*
    Tests from pos = 1 --> 30
   */
  //printf("%d\n", occ(1,'d'));
  assert(occ(1,'d')==1);
  assert(occ(2,'d')==2);
  assert(occ(1,'t')==0);
  assert(occ(2,'t')==0);
  assert(occ(3,'t')==1);
  /*
   * Tests in the middle (7xxx)
   */
  //printf("f: %d\n", occ(9329, 'f'));
  assert(occ(9387,'2')==148);
  assert(occ(9387,']')==219);
  assert(occ(9387,'b')==52);

  assert(occ(12107,'f')==97);
  assert(occ(12107,'m')==196);
  assert(occ(12107,' ')==1484);
  /*
   * Tests at final position 13072
   */
  assert(occ(bwtSize,'a')==645);
  assert(occ(bwtSize,'.')==141);
  assert(occ(bwtSize,'r')==593);
  assert(occ(bwtSize,'s')==526);
  printf("ALL TESTS PASSED FOR YAHOO.BWT\n");  
  
  



  fclose(indexFile);
  return 0;
}

/*
 * Occurence function
 * if pos is LESS THAN 1024, fread pos characters from bwtFile
 * else fseek to (rd.pos/1024 - 1) * sizeInt * 128 + c * sizeInt
 * read that value, then read remaining X characters from bwtFile
 */
int occ(int pos, char c) {
  /*
   * pos is LESS THAN 1024
   * Read the first pos characters into bwtStream
   */
  if (pos < 1024) {
    fseek(bwtFile, 0, SEEK_SET);
    char bwtStream[1024];
    assert((fread(bwtStream, 1, pos, bwtFile)) == pos);
    
    /*
     * bwtStream[0..pos-1] now contains first pos characters of bwtFile
     * Count all the occurences of c from 0...pos-1
     */
    int occurence = 0;
    int i;
    for (i = 0; i < pos; i++) {
      if (bwtStream[i] == c) {
	occurence++;
      }
    }
    return occurence;
  }
  
  /*
   * pos >= 1024
   */
  
  int row = ((int)floor(pos/1024.0)) - 1;
  int posInIndex = row*sizeof(int)*128 + ((int)c)*sizeof(int);
  fseek(indexFile, posInIndex, SEEK_SET);
  int smallOcc[1];
  assert(fread(smallOcc, sizeof(int), 1, indexFile) == 1);
  int occurence = smallOcc[0];
  int nthCharInBwt = ((int)floor(pos/1024.0))*1024;
  /*
   * If occ(pos) is stored in index file, return it
   */
  if (nthCharInBwt == pos) {
    return occurence;
  }
  int numCharsToRead = pos - nthCharInBwt;
  char bwtStream[1024];
  fseek(bwtFile, nthCharInBwt, SEEK_SET);
  assert(fread(bwtStream, 1, numCharsToRead, bwtFile) == numCharsToRead);
  int i;
  
  /*
   * Update occurence 
   */
  for (i = 0; i < numCharsToRead; i++) {
    if(bwtStream[i] == c) {
      occurence++;
    }
  }
  return occurence;
}
/*
 * File is >= 1024 bytes. Generate the index file, name it zzz.bwt.ind
 */
void gen_index_file(char *name) {
  /*
   * Generate name of index file
   */
  char ifName[100];
  char *ind = ".ind";
  strcpy(ifName, name);
  strcat(ifName, ind);
  
  //Create file name xxx.bwt.ind and write to it
  FILE *fp = fopen(ifName, "wb");
  
  /*
   * Create temporary occ[128]
   * Read 1024 characters from BWT into charSet[1024]
   * for each c in charSet increase occ[c] by 1
   * write occ to indexFile
   * keep doing this until you read less than 1024 from BWT, in which case
   * we're done
   */
  int i;
  int tempOcc[128] = {0};
  char charSet[1024];
  fseek(bwtFile, 0, SEEK_SET);
  while ( fread(charSet, sizeof(char), 1024, bwtFile) == 1024 ) {
    for (i = 0; i < 1024; i++) {
      tempOcc[(int)charSet[i]]++;
    }
    fwrite(tempOcc, sizeof(int), 128, fp);
  }
  
  fclose(fp);

}



