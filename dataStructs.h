typedef struct {
  int first, last;
} first_last_t;

typedef struct {
  int pos, number;
} record_t;

typedef record_t item_t;
typedef struct node *SL;

SL newList(void);
void insert(SL, item_t);
item_t get(SL, int);
int size(SL);
void printList(SL);
void deleteList(SL);
void delete(SL, int);
int isEmpty(SL);
