#include "dataStructs.h"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

struct node {
  item_t item;
  SL next;
};

int isEmpty(SL list) {
  if (list->next == NULL) {
    return 1;
  } else {
    return 0;
  }
}

SL newList(void) {
  SL list = malloc(sizeof(*list));
  assert(list != NULL);
  list->next = NULL;
  return list;
}

void insert(SL list, item_t item) {
  assert(list != NULL);
  /*
   * Empty list case
   */
  if (list->next == NULL) {
    SL new = malloc(sizeof(*new));
    new->item = item;
    new->next = NULL;
    list->next = new;
    return;
  }

  /*
   * List is not empty
   * Insert it in order
   */
  
  SL old = list;
  SL curr = list->next;
  
  while (curr != NULL) {
    if (curr->item.number >= item.number) {
      if (curr->item.number == item.number) {
	return;
      }
      SL new = malloc(sizeof(*new));
      new->item = item;
      old->next = new;
      new->next = curr;
      return;
    }
    
    /*
     * The current node is smaller than new. New goes AFTER curr
     */
    old = curr;
    curr = curr->next;
  }

  /*
   * Reaching here means that curr == NULL
   * Insert new at end of list
   */
  SL new = malloc(sizeof(*new));
  new->item = item;
  new->next = NULL;
  old->next = new;
  return;
}

void delete(SL list, int pos) {
  assert(list != NULL);
  assert(pos < size(list));
  assert(pos >= 0);

  int currPos = 0;
  SL old = list;
  SL curr = list->next;
  
  while (currPos != pos) {
    old = curr;
    curr = curr->next;
    currPos++;
  }
  
  old->next = curr->next;
  free(curr);
  
  return;
}




/*
 * Array indexes (start from 0)
 */
item_t get(SL list, int pos) {
  assert(list != NULL);
  assert(pos < size(list));
  assert(pos >= 0);
  
  int currPos = 0;
  SL curr = list->next;
  while (currPos != pos) {
    curr = curr->next;
    currPos++;
  }
  return curr->item;
}
  
int size(SL list) {
  assert(list != NULL);
  int size = 0;
  SL curr = list->next;
  
  while (curr != NULL) {
    size++;
    curr = curr->next;
  }

  return size;
} 
  
void printList(SL list) {
  assert(list != NULL);
  if (size(list) == 0) {
    printf("Empty List\n");
  } else {
    SL curr = list->next;
    while (curr != NULL) {
      printf("[%d %d] -> ", curr->item.pos, curr->item.number);
      curr = curr->next;
    }
    printf("X\n");
  }
  return;
}

void deleteList(SL list) {
  assert(list != NULL);
  //Empty list case
  if (list->next == NULL) {
    free(list);
    return;
  }

  SL old = list;
  SL curr = list->next;
  
  while (curr != NULL) {
    free(old);
    old = curr;
    curr = curr->next;
  }
  free(old);
  return;
}
