#include <stdio.h>
#include "dataStructs.h"
#include <assert.h>

int main() {
  SL l = newList();
  assert(l != NULL);
  record_t r;
  
  assert(size(l) == 0);
  
  r.number = 1;
  insert(l, r);
  assert(size(l) == 1);
  
  r.number = 10;
  insert(l, r);
  assert(size(l) == 2);

  r.number = 5;
  insert(l, r);
  assert(size(l) == 3);


  r.number = 35;
  insert(l, r);
  assert(size(l) == 4);

  r.number = 1;
  insert(l, r);
  assert(size(l) == 4);
  
  r.number = 5;
  insert(l, r);
  assert(size(l) == 4);

  r.number = 10;
  insert(l, r);
  assert(size(l) == 4);

  r.number = 35;
  insert(l, r);
  assert(size(l) == 4);

  r.number = 20;
  insert(l, r);
  assert(size(l) == 5);
  
  r.number = 19;
  insert(l, r);
  assert(size(l) == 6);
  
  assert(get(l, 0).number == 1);
  assert(get(l, 1).number == 5);
  assert(get(l, 2).number == 10);
  assert(get(l, 3).number == 19);
  assert(get(l, 4).number == 20);
  assert(get(l, 5).number == 35);
  
  //Test delete
  delete(l, 2);
  assert(size(l) == 5);
  assert(get(l, 2).number == 19);
  
  delete(l, 0);
  assert(size(l) == 4);
  assert(get(l, 0).number == 5);

  printList(l);
  printf("All tests passed\n");
  
  deleteList(l);
  printList(l);

  return 0;
}
