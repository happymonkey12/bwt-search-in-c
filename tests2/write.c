#include <stdio.h>

int main() {
  FILE *fp = fopen("test.index", "wb");
  if (fp == NULL) {
    printf("Failed to open file\n");
    return 0;
  }

  printf("Writing numbers to file\n");
  int arr[] = {1,2,3,4,5};
  fwrite(arr, sizeof(int), 5, fp);
  fclose(fp);

  fp = fopen("test.index", "rb");
  if (fp == NULL) {
    printf("Failed to open file\n");
    return 0;
  }
  
  printf("Reading file\n");
  int readArr[100];
  int itemsRead = fread(readArr, sizeof(int), 5, fp);
  fseek(fp, -2*sizeof(int), SEEK_CUR);
  printf("%d\n",fread(readArr, sizeof(int), 5, fp));
  fclose(fp);
  printf("Num items read: %d\nItems: ", itemsRead);
  int i = 0;
  for (i = 0; i < itemsRead; i++) {
    printf("%d ", readArr[i]);
  }
  printf("\n");
  
  
  return 0;
}
